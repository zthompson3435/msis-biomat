const donorDataExample = [
    {
        firstName:  "Bob",
        lastName:   "Smith",
        age: "25",
        location: "Salt Lake City",
        donationDate: "1/22/22",
        compensation: "25.00",
        donationQtyPints: "2",
    },
    {
        firstName:  "Marty",
        lastName:   "Robinson",
        age: "25",
        location: "Orem",
        donationDate: "2/12/22",
        compensation: "15.00",
        donationQtyPints: "1.5",
    },
    {
        firstName:  "Bob",
        lastName:   "Smith",
        age: "25",
        location: "Salt Lake City",
        donationDate: "1/22/22",
        compensation: "25.00",
        donationQtyPints: "2",
    },
    {
        firstName:  "Marty",
        lastName:   "Robinson",
        age: "25",
        location: "Orem",
        donationDate: "2/12/22",
        compensation: "15.00",
        donationQtyPints: "1.5",
    }
];

const donorAppts = [
    {
        location: "Salt Lake City",
        donationDate: "1/22/22",
        compensation: "",
        donationQtyPints: "",
    },
    {
        location: "Orem",
        donationDate: "2/12/22",
        compensation: "15.00",
        donationQtyPints: "1.5",
    },
    
];


const personnelAppts = [
    {
        firstName:  "Bob",
        lastName:   "Smith",
        age: "25",
        location: "Salt Lake City",
        donationDate: "1/22/22",
        compensation: "25.00",
        donationQtyPints: "2",
    },
    {
        firstName:  "Marty",
        lastName:   "Robinson",
        age: "25",
        location: "Orem",
        donationDate: "2/12/22",
        compensation: "15.00",
        donationQtyPints: "1.5",
    },
    {
        firstName:  "Bob",
        lastName:   "Smith",
        age: "25",
        location: "Salt Lake City",
        donationDate: "1/22/22",
        compensation: "25.00",
        donationQtyPints: "2",
    },
    {
        firstName:  "Marty",
        lastName:   "Robinson",
        age: "25",
        location: "Orem",
        donationDate: "2/12/22",
        compensation: "15.00",
        donationQtyPints: "1.5",
    }
];