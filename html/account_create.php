<?php

?>


<html>
	<!-- HEADER -->
	<?php include "./partials/header.partial.php" ?>
	<body>
        <!-- NAV -->
	    <?php include "./partials/nav.partial.php" ?>
	
        <div class="container mt-5">
            <!-- PAGE CONTENT HERE -->
            <!-- PAGE CONTENT HERE -->        

            <div class="row">
				<div class="col-md-8 offset-md-2 mt-5">
					<div class="bg-light p-5 border shadow">
						<h1 class="mb-3 text-primary">Create Account</h1>
						<!-- Login Form -->
						<form>                
							<div class="mb-4">
                                <label for="username">Username</label>
								<input name="username" id="username" type="email" class="form-control" placeholder="Username/Email">
							</div>
                            <div class="d-flex mb-4">
                                <div class="mr-2 flex-fill">
                                    <label for="fname">First Name</label>
                                    <input name="fname" id="fname" type="text" class="form-control" placeholder="First Name">
                                </div>
                                <div style="width:2rem;"></div>
                                <div class="flex-fill">
                                    <label for="lname">Last Name</label>
                                    <input name="lname" id="lname" type="text" class="form-control" placeholder="Last Name">
                                </div>
                            </div>
							<div class="mb-4">
                                <label for="password">Password</label>
								<input name="password" id="password" type="password" class="form-control" placeholder="Enter Password">
							</div>
                            <div class="mb-4">
                                <label for="confirmPassword">Create Password</label>
								<input name="confirmPassword" id="confirmPassword" type="password" 
                                    class="form-control" placeholder="Confirm Password">
							</div>
							<div class="mb-4 form-check w-100">        
								<label class="form-check-label">
								<input type="checkbox" class="form-check-input"> Remember Me
								</label>
								<a href="#" class="float-end">Reset Password</a>
							</div>
							<button type="submit" class="btn btn-primary w-100 my-3 shadow">Login</button>
							<p class="text-center m-0">Already have an account, <a href="login.php">Please Login</a></p>
						</form>
						<!-- Login Form -->
					</div>
				</div>	
			</div> <!--end-row-->
            
            <!-- END PAGE CONTENT -->
            <!-- END PAGE CONTENT -->
        </div>

        <!-- FOOTER -->
        <?php include "./partials/footer.partial.php" ?>

	</body>	
</html>







