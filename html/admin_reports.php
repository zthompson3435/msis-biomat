<?php

?>


<html>
	<!-- HEADER -->
	<?php include "./partials/header.partial.php" ?>
	<body>
        <!-- NAV -->
	    <?php include "./partials/nav.partial.php" ?>
	
        <div class="container mt-5">
            <!-- PAGE CONTENT HERE -->
            <!-- PAGE CONTENT HERE -->        

            <div class="row">
                    
                <h1 class=" mt-40 text-primary">Admin Reports</h1><br>

                <label for="cars">Choose a Month:</label>
                <div class="card chart-container" id="chart-container">
                    <canvas id="chart"></canvas>
                </div>
                <select name="months" id="chooseMonth">
                    <option value="1">Jan</option>
                    <option value="2">Feb</option>
                    <option value="3">Mar</option>
                    <option value="4">Apr</option>
                    <option value="5">May</option>
                    <option value="6">Jun</option>
                    <option value="7">Jul</option>
                    <option value="8">Aug</option>
                    <option value="9">Sep</option>
                    <option value="10">Oct</option>
                    <option value="11">Nov</option>
                    <option value="12">Dec</option>
                </select>
                
            </div>
            
            <!-- END PAGE CONTENT -->
            <!-- END PAGE CONTENT -->
        </div>

        <!-- FOOTER -->
        <?php include "./partials/footer.partial.php" ?>

        <!-- CHART JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
        <script>
            const ctx = document.getElementById("chart").getContext('2d');
            function generateChart(dataSet) {
                const graphObj = {
                    type: 'line',
                    data: {
                    labels: ["sunday", "monday", "tuesday",
                    "wednesday", "thursday", "friday", "saturday"],
                    datasets: [{
                        label: 'Last week',
                        backgroundColor: 'rgba(161, 198, 247, 1)',
                        borderColor: 'rgb(47, 128, 237)',
                        data: dataSet,
                    }]
                    },
                    options: {
                    scales: {
                        yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                        }]
                    }
                    },
                }
                return new Chart(ctx, graphObj);
            }
            let dataSet = [3000, 4000, 2000, 5000, 8000, 9000, 2000];
            let myChart = generateChart(dataSet);

            document.addEventListener('input', function (event) {
                // document.getElementById("chart").remove();
                // const node = document.createElement("canvas");
                // node.id = "chart";
                // document.getElementById("chart-container").appendChild(node);
                
                // Only run on our select menu
                if (event.target.id !== 'chooseMonth') return;
                

                // Do stuff...
                const monthVal = event.target.value;
                
                if (monthVal === "2") {
                    dataSet = [1500, 2000, 3000, 2000, 8000, 3000, 2000]
                } else if (monthVal === "3") {
                    dataSet = [6000, 1000, 7000, 2000, 8000, 3000, 2000]
                } else {
                    dataSet = [3000, 4000, 2000, 3000, 3000, 3000, 2000];
                }
                myChart = generateChart(dataSet);


            }, false);

            
            
            
        </script>

	</body>	
</html>







