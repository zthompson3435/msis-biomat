<?php

?>


<html>
	<!-- HEADER -->
	<?php include "./partials/header.partial.php" ?>
	<body>
        <!-- NAV -->
	    <?php include "./partials/nav.partial.php" ?>
	
        <div class="container mt-5">
            <!-- PAGE CONTENT HERE -->
            <!-- PAGE CONTENT HERE -->        

            <div class="row">
				<div class="col-md-8 offset-md-2 mt-5">
					<div class="bg-light p-5 border shadow">
						<h1 class="mb-3 text-primary">Contact Support</h1>
						<!-- Support Ticket/Cases Form -->
						<form>
						<select name="supportIssueType" id="supportIssueType">
							<option value="1">Scheduling</option>
							<option value="2">Donation</option>
							<option value="3">Facility</option>
							<option value="4">Staff</option>
							<option value="5">Other</option>
						</select>               
							<div class="mb-4">
                                <label for="title">Title</label>
								<input name="title" id="title" type="text" class="form-control" placeholder="Desription Title">
							</div>
							<div class="mb-4">
                                <label for="description">Describe the issue:</label>
								<textarea name="description" id="description" rows="4" class="form-control" placeholder="Provide a brief description of the problem"></textarea>
							</div>
                            
							<button type="submit" class="btn btn-primary w-100 my-3 shadow">Submit</button>
							
						</form>
						<!-- Login Form -->
					</div>
				</div>	
			</div> <!--end-row-->
            
            <!-- END PAGE CONTENT -->
            <!-- END PAGE CONTENT -->
        </div>

        <!-- FOOTER -->
        <?php include "./partials/footer.partial.php" ?>

	</body>	
</html>







