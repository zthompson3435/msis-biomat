<?php

?>


<html>
	<!-- HEADER -->
	<?php include "./partials/header.partial.php" ?>
	<body>
        <!-- NAV -->
	    <?php include "./partials/nav.partial.php" ?>

		<!-- CAROUSEL -->
		<div id="carousel" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img class="d-block w-100" src="../assets/images/carousel1.jpg" alt="plane1" style="width:100%;">
					</div>
				</div>
			</div>
	
        <div class="container mt-5">
            <!-- PAGE CONTENT HERE -->
            <!-- PAGE CONTENT HERE -->        

            <div class="row">
				
				

				<div id="about" class="container-fluid">
					<div class="row">
						<div class="col-sm-8">
							<h2>Biomat USA</h2><br>
							<h4>Blood plasma is used to manufacture medicines that are used in hospitals for surgical and traumatic events, for supplementing the immune system of burn victims and those with compromised immune systems so they can feel better, get stronger, recover, and live a longer healthier life.</h4><br>
							<p>Biomat USA (previously SeraCare, and now a wholly-owned subsidiary of Grifols) is a global healthcare company that employees over 13,000 employees over the span of 25 countries around the globe. Founded in 1940, Biomat has been on the forefront of the plasma industry and has pioneered many of the sophisticated techniques used today for the collection, testing, and manufacturing of plasma-derived medicines.</p>
							<p>While the plasma donations collected by Biomat at its many international locations is invaluable in medicinal use for such therapeutic areas as immunology, neurology, shock and trauma, and infectious diseases, the primary service provided by Biomat that is being focused on is the collection of plasma donations by donors at over one hundred Biomat donation centers. <a href='locations.php'>Find a donation center.</a></p>
						</div>
					</div>
				</div>

            </div>
            
            <!-- END PAGE CONTENT -->
            <!-- END PAGE CONTENT -->
        </div>

        <!-- FOOTER -->
        <?php include "./partials/footer.partial.php" ?>

	</body>	
</html>








	