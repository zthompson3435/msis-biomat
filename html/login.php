<?php

?>


<html>
	<!-- HEADER -->
	<?php include "./partials/header.partial.php" ?>
	<body>
        <!-- NAV -->
	    <?php include "./partials/nav.partial.php" ?>
	
        <div class="container mt-5">
            <!-- PAGE CONTENT HERE -->
            <!-- PAGE CONTENT HERE -->        


			<div class="row">
				<div class="col-md-8 offset-md-2 mt-5">
					<div class="bg-light p-5 border shadow">
						<h1 class="mb-3 text-primary">Login</h1>
						<!-- Login Form -->
						<form>                
							<div class="mb-4">
                                <label for="username">Username</label>
								<input name="username" id="username" type="email" class="form-control" placeholder="Username/Email">
							</div>
                            
							<div class="mb-4">
                                <label for="password">Password</label>
								<input name="password" id="password" type="password" class="form-control" placeholder="Enter Password">
							</div>
                            
							<div class="mb-4 form-check w-100">        
								<label class="form-check-label">
								<input type="checkbox" class="form-check-input"> Remember Me
								</label>
								<a href="#" class="float-end">Reset Password</a>
							</div>
							<a href="locations.php" class="btn btn-primary">
								<!-- <button  class="btn btn-primary w-100 my-3 shadow">Login</button> -->
								Login
							</a>
							<p class="text-center m-0">Don't have an account, <a href="account_create.php">sign up</a></p>
						</form>
						<!-- Login Form -->
					</div>
				</div>	
			</div> <!--end-row-->
            
            <!-- END PAGE CONTENT -->
            <!-- END PAGE CONTENT -->
        </div>

        <!-- FOOTER -->
        <?php include "./partials/footer.partial.php" ?>

	</body>	
</html>












	