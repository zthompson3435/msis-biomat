<?php

?>


<html>
	<!-- HEADER -->
	<?php include "./partials/header.partial.php" ?>
	<body>
        <!-- NAV -->
	    <?php include "./partials/nav.partial.php" ?>
	
        <div class="container mt-5">
            <!-- PAGE CONTENT HERE -->
            <!-- PAGE CONTENT HERE -->        

            <div class="row">
                <h1 class=" mt-40 text-primary">Donor: Appt / Compensation Dashboard</h1><br>
                <!-- DATA TABLE -->
                <?php include "./partials/data_table.partial.php" ?>
            </div>
            
            <!-- END PAGE CONTENT -->
            <!-- END PAGE CONTENT -->
        </div>

        <!-- FOOTER -->
        <?php include "./partials/footer.partial.php" ?>
        <script
            src="https://code.jquery.com/jquery-3.6.0.slim.min.js"
            integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI="
            crossorigin="anonymous">
        </script>
        <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"></script>
        <script src="../assets/scripts/data-table-example.js"></script>
        <script>
            $(document).ready(function() {
                donorAppts.forEach(donor => {
                    $('#dataTableContainer').append($(
                        `   <tr>
                                <td>${donor.location}</td>
                                <td>${donor.donationDate}</td>
                                <td>${donor.compensation}</td>
                                <td>${donor.qtyPints}</td>
                                <td>
                                    <a href="appointment_create.php">
                                        <button class="btn btn-primary btn-outline">Edit</button>
                                    </a>
                                </td>
                                <td><button class="btn btn-secondary btn-outline">Delete</button></td>
                            </tr>
                        `
                    ));
                })
                

                $('#example').DataTable( {
                    columnDefs: [ {
                        targets: [ 0 ],
                        orderData: [ 0, 1 ]
                    }, {
                        targets: [ 1 ],
                        orderData: [ 1, 0 ]
                    }, {
                        targets: [ 4 ],
                        orderData: [ 4, 0 ]
                    } ]
                } );
            } );
        </script>


	</body>	
</html>









