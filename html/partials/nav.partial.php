<!-- Navbar -->
<?php 
 
?>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <div class="container">
    <a class="navbar-brand" href="#">BIOMAT</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
        <ul class="navbar-nav me-2">

            <!-- ADD ADDITIONAL NAV LINKS HERE -->

            <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="home.php">Home</a>
            </li>
        

            <!-- Contact / Support / Locations -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Contact
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <li><a class="dropdown-item" href="contact.php">Contact Us</a></li>
                    <li><a class="dropdown-item" href="support_case_create.php">Support</a></li>
                    <li><a class="dropdown-item" href="locations.php">Locations</a></li>
                </ul>
            </li>
            
            <!-- User Account -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Account
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <li><a class="dropdown-item" href="account_create.php">Create Account</a></li>
                    <li><a class="dropdown-item" href="account_update.php">Update Account</a></li>
                    <li><a class="dropdown-item" href="login.php">Login</a></li>
                </ul>
            </li>

            <!-- ADMIN -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Admin
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <li><a class="dropdown-item" href="admin_dashboard.php">User Management Dashboard</a></li>
                    <li><a class="dropdown-item" href="admin_reports.php">User Reports</a></li>
                </ul>
            </li>

            <!-- PERSONNEL -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Personnel
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <li><a class="dropdown-item" href="personnel_dashboard.php">Appts Dashboard</a></li>
                    <li><a class="dropdown-item" href="personnel_reports.php">Reports</a></li>
                </ul>
            </li>

        

            <!-- Donor -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Donor
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <li><a class="dropdown-item" href="donor_dashboard.php">Dashboard</a></li>
                    <li><a class="dropdown-item" href="donor_reports.php">Reports</a></li>
                    <li><a class="dropdown-item" href="appointment_create.php">Schedule</a></li>
                </ul>
            </li>

            <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="login.php">Login</a>
            </li>

        <!-- END ADDITIONAL NAV LINKS -->
      </ul>
    </div>
  </div>
</nav>