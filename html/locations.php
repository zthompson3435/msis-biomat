<?php

?>


<html>
	<!-- HEADER -->
	<?php include "./partials/header.partial.php" ?>
	<body>
        <!-- NAV -->
	    <?php include "./partials/nav.partial.php" ?>
	
        <div class="container mt-5">
            <!-- PAGE CONTENT HERE -->
            <!-- PAGE CONTENT HERE -->        

            <div class="row">
                    <h1 class=" mt-40 text-primary">Locations</h1>
                    <div class="d-flex">

                        <div class="card w-25">
                            <div class="card-body">
                                <img class="w-100" src="../assets/images/clinic.jpg" />
                                <h5 class="card-title">Donation Center</h5>
                                <p class="card-text">Ogden, UT</p>
                                <a href="appointment_create.php" class="btn btn-primary">Schedule</a>
                            </div>
                        </div>

                        <div class="card w-25">
                            <div class="card-body">
                                 <img class="w-100" src="../assets/images/clinic.jpg" />
                                <h5 class="card-title">Donation Center</h5>
                                <p class="card-text">Murray, UT</p>
                                <a href="appointment_create.php" class="btn btn-primary">Schedule</a>
                            </div>
                        </div>

                        <div class="card w-25">
                            <div class="card-body">
                                <img class="w-100" src="../assets/images/clinic.jpg" />
                                <h5 class="card-title">Donation Center</h5>
                                <p class="card-text">Bountiful, UT</p>
                                <a href="appointment_create.php" class="btn btn-primary">Schedule</a>
                            </div>
                        </div>

                        <div class="card w-25">
                            <div class="card-body">
                                <img class="w-100" src="../assets/images/clinic.jpg" />
                                <h5 class="card-title">Donation Center</h5>
                                <p class="card-text">Orem, UT</p>
                                <a href="appointment_create.php" class="btn btn-primary">Schedule</a>
                            </div>
                        </div>

                    </div>
                    
            </div>
            
            <!-- END PAGE CONTENT -->
            <!-- END PAGE CONTENT -->
        </div>

        <!-- FOOTER -->
        <?php include "./partials/footer.partial.php" ?>

	</body>	
</html>







